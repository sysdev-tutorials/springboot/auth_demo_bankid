## Java Spring project + authentication with BankID

Run the project with 
```
mvn spring-boot:run
```

Open the browser, ```http://localhost:8080/login```.

When BankID credentials are asked, enter the following info. 

Personal number ```29090816894```

One time password: ```otp```

Password: ```qwer1234```

Note that the BankID service used in this example is provided by Signicat´s preprod environment (www.signicat.com)
That means, only test BankID credentials will work and real BankID credentials will not work. 

Note also that test users are retrieved from: https://developer.signicat.com/identity-methods/nbid/demo-nbid/#test-users

## OpenID connect implementation details

1. Upton login send ```authorization``` request to ```/authorize``` endpoint. This will trigger BankID authentication with the User.
2. Upon successful user authentication, this backend will receive a callback (on ```/redirect``` endpoint on this example) with ```authorization code```
3. The backend then gets ```access_token``` (and ```refresh_token```) by sending HTTP request to ```/token``` endpoint, and providing ```authorization code``` received in earlier step
4. Then the backend will take care of the ```access_token``` for the user session