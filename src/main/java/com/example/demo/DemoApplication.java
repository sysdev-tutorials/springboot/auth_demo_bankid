package com.example.demo;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.simple.JSONObject;
import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@SpringBootApplication
@RestController
public class DemoApplication {

	@GetMapping("/")
	String home() {
		return "Spring is here!";
	}

	@GetMapping("/login")
	String login(HttpServletRequest request, HttpServletResponse response) {
		try {
			response.sendRedirect("https://preprod.signicat.com/oidc/authorize?response_type=code&scope=openid+profile+signicat.national_id&client_id=demo-preprod&redirect_uri=http%3A%2F%2Flocalhost%3A8080%2Fredirect&acr_values=urn:signicat:oidc:method:nbid&state=nbid:auth_demo_bankid:123456789");
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "Spring is here!";
	}

	@GetMapping("/redirect")
	@ResponseBody String consumeCallback(@RequestParam(value="state", required=true)  String state, HttpServletRequest request, HttpServletResponse response) throws Exception {

		Map<String, String []> paramMap = request.getParameterMap();

		if(paramMap.containsKey("error")) {
			System.err.println("Error: " + paramMap.get("error_description"));
		}

		if(paramMap.containsKey("code")) {
			String code = paramMap.get("code")[0];

            try {
                // get token
                String tokenUrl = "https://preprod.signicat.com/oidc/token";
                HttpPost httpPost = new HttpPost(tokenUrl);
                httpPost.setHeader(HttpHeaders.ACCEPT, ContentType.APPLICATION_JSON.toString());
                httpPost.setHeader(HttpHeaders.AUTHORIZATION, "Basic " + "ZGVtby1wcmVwcm9kOm1xWi1fNzUtZjJ3TnNpUVRPTmI3T240YUFaN3pjMjE4bXJSVmsxb3VmYTg=");
                List<NameValuePair> params = new ArrayList<NameValuePair>();
                params.add(new BasicNameValuePair("client_id", "demo-preprod"));
                params.add(new BasicNameValuePair("redirect_uri", "http://localhost:8080/redirect"));
                params.add(new BasicNameValuePair("grant_type", "authorization_code"));
                params.add(new BasicNameValuePair("code", code));
                httpPost.setEntity(new UrlEncodedFormEntity(params, StandardCharsets.UTF_8));
                final String content = postRequest(httpPost);
                // parse response
                JSONObject jsonObject = new ObjectMapper().readValue(content, JSONObject.class);
                String accessToken = (String) jsonObject.get("access_token");
                String idToken = (String)  jsonObject.get("id_token");

                //get userinfo
                HttpPost httpPost_userinfo = new HttpPost("https://preprod.signicat.com/oidc/userinfo");
                httpPost_userinfo.setHeader(HttpHeaders.ACCEPT, ContentType.APPLICATION_JSON.toString());
                httpPost_userinfo.setHeader(HttpHeaders.AUTHORIZATION, "Bearer " + accessToken);
                final String content_userinfo = postRequest(httpPost_userinfo);

                // print
                return "<html>" +
                        "User is logger in!!" +
                        "<br>" +
                        "<br>" +
                        "Access Token: " + accessToken +
                        "<br>" +
                        "<br>" +
                        "ID Token: " + idToken +
                        "<br>" +
                        "<br>" +
                        "Userinfo: " + content_userinfo+
                        "</html>";
            } catch (final Exception e) {
                throw new Exception(e.getMessage());
            }
		}

		return "Problems in logging in!!";

        // NOTE. In a real-world scenario, this token will be used for further user session management in the application
	}

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}


    // CSOFF: CyclomaticComplexity Leave it
    protected String postRequest(final HttpPost httpPost) throws Exception {
        try {
            CloseableHttpClient httpClient = HttpClientBuilder.create().useSystemProperties().build();
            CloseableHttpResponse httpResponse = httpClient.execute(httpPost);
            final int status = httpResponse.getStatusLine().getStatusCode();
            if (status == HttpStatus.SC_FORBIDDEN || status / 100 != 2) {
                throw new Exception("Something went wrong!! Handle this properly!!!");
            }
            final String content = EntityUtils.toString(httpResponse.getEntity(), StandardCharsets.UTF_8);
            return content;
        } catch (final Exception e) {
            throw new Exception(e.getMessage());
        }
    }
}
